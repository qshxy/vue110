//导入处理路径的模块
const path = require('path');
//导入在内存中生成的html页面的插件
/* 这个插件的两个作用
1.自动在内存中根据指定的页面生成一个内存页面
2.自动把打包好的bundle.js追加到页面中去 */
const htmlWebpackPlugin = require('html-webpack-plugin');
// 这个配置文件就是，其实就是一个js文件，通过node中的模块操作，向外暴露一个配置对象
//导出一个配置对象，将来在webpack启动的时候，会默认查找webpack.config.js，并读取这个文件中导出的
//这个配置对象，来进行打包处理
module.exports = {
    mode: 'development',
    //在配置文件中需要手动配置入口和出口，
    entry: path.join(__dirname, './src/main.js'), //入口，需要webpack打包的那个文件
    output: { //输出文件相关配置
        path: path.join(__dirname, '/dist'), //指定好打包好的文件，输出到那个目录中去
        filename: 'bundle.js' //这是指定输出的文件名
    },
    plugins: [ //只要是插件都要放到pligins节点中
        new htmlWebpackPlugin({ //创建一个在内存中生成htnl页面插件
            template: path.join(__dirname, './src/index.html'),
            /* 指定模板页面，将来会根据指定的页面
                     路径，去生成内存中的页面 */
            filename: 'index.html' //指定生成的页面

        })
    ],
    module: { //这个节点用于配置所有第三方加载器
        rules: [ //所有第三方模块的匹配规则

            { //配置处理css文件的第三方loader规则
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }, //test文件的后缀名匹配
            { //配置处理less文件的第三方loader规则
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader']
            },
            { //配置处理scss文件的第三方loader规则
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            { //配置处理img的url文件的第三方loader规则
                test: /\.(png|jpg|gif|bmp)$/,
                use: 'url-loader?limit=7631&name=[hash:8]-[name].[ext]' //处理图片的loader
                //limit给定的值是图片的大小，单位byte，如果我们引用的图片大小大于或等于limit值
                /*   则不会被转为base64格式的字符串，如果我们引用的图片大小小于limit值
                  则会被转为base64格式的字符串
                 [hash: 8]值为了区分重名的图片，[name] 之前叫什么名还是什么名，[ext] 之前什么格式就是什么格式  */
            },
            { //配置处理字体文件的第三方loader规则
                test: /\.(ttf|eot|svg|woff|woff2)$/,
                use: 'url-loader',
            },
            { //配置处理高级js语法的第三方loader规则
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                use: 'vue-loader'
            }, // 处理 .vue 文件的 loader
            {//处理图片缩略图
                test: /vue-preview.src.*?js$/,
                loader: 'babel'

            }
        ]
    },
    resolve: {
        alias: { //修改vue导入时候的路径
            /* "vue$":"vue/dist/vue.js" */
        }
    },
      devServer: {
         //我们在这里对webpack-dev-server进行配置
      //   contentBase: "./",
       //  historyApiFallback: true,
      //   inline: true,
        // hot: true
        disableHostCheck: true,
       /*  public: 'local.kingsum.biz' */
     } 
}
/* 当我们在控制台直接输入webpack命令执行的时候，webpack做了以下几步：
1.首先webpack发现我们没有通过输入命令的形式给他指定入口和出口
2.webpack就会去项目的根目录中查找一个叫做webpack.config.js的配置文件
3.当找到配置文件后，webpack会去解析执行这个配置文件，当解析执行完配置文件后，就得到了配置文件
中导出的配置对象
4.当webpack拿到配置对象后，就拿到了配置对象中指定的入口和出口，然后进行打包构建 */


// 使用webpack-dev-server来实现代码实时打包编译，当修改代码之后，会自动进行打包构建。
//1.运行运行cnpm i webpack-dev-server -D把这个工具安装到项目本地开发依赖
//2.安装完毕后，这个工具的用法和webpack的用法完全一样，
//3.由于在项目中我们是本地安装的webpack-dev-server，所以无法把它当做脚本命令，在powershell终端直接运行
//（只有那些安装到全局-g工具才能在终端中正常执行）
//cnpm i装包，项目本地要安装webpack，
//4.注意，如果想运行webpack-dev-server，要求本地项目中安装webpack 执行命令cnpm i webpack -D
//还需要在package.json中的script标签中定义命令 "start": "webpack-dev-server",
//所以运行项目时直接npm start
//webpack-dev-server帮我们打包的bundle.js文件，并没有存放到实际的物理磁盘中，而是直接托管到电脑的内存中，
/*5 所以现在根目录中根本找不到打包好的bundle.js文件
6. 我们也可以认为webpack - dev - server 把打包好的文件，以一种虚拟的形式托管到我们的根目录下，
虽然我们看不到它，但是可以认为和dist src node_modules同级，有一个看不见的文件叫bundle.js*/