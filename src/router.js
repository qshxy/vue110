import VueRouter from "vue-router";
//导入需要的组件
import HomeContainer from "./component/tabar/HomeContainer.vue";
import MemberContainer from "./component/tabar/MemberContainer.vue";
import SearchContainer from "./component/tabar/SearchContainer.vue";
import ShopcarContainer from "./component/tabar/ShopcarContainer.vue";
import NewsList from "./component/news/NewsList.vue";
import NewsInfo from "./component/news/NewsInfo.vue";
import PhotoList from "./component/photos/PhotoList.vue";
import PhotoInfo from "./component/photos/PhotoInfo.vue";
import GoodsList from './component/goods/GoodsList.vue';
import GoodsInfo from './component/goods/GoodsInfo.vue';
import GoodsDesc from './component/goods/GoodsDesc.vue';
import GoodsComment from './component/goods/GoodsComment.vue';
import MessageBack from './component/messageback/MessageBack.vue';
import Videozq from './component/videozq/Videozq.vue';
import ContactUs from './component/contactus/ContactUs.vue'

//4.创建路由对象
var router = new VueRouter({
  /*    mode: 'history', */
  routes: [{
      path: "/",
      name: "HomeContainer",
      redirect: "/home"
    },
    {
      path: "/home",
      name: "HomeContainer",
      component: HomeContainer,
      meta: {
        title: '首页'
      }
    },
    {
      path: "/member",
      name: "MemberContainer",
      component: MemberContainer,
       meta: {
         title: '会员'
       }
    },
    {
      path: "/shopcar",
      name: "ShopcarContainer",
      component: ShopcarContainer,
        meta: {
          title: '购物车'
        }
    },
    {
      path: "/search",
      name: "SearchContainer",
      component: SearchContainer,
        meta: {
          title: '搜索'
        }
    },
    {
      path: "/home/newsList",
      name: "NewsList",
      component: NewsList,
        meta: {
          title: '新闻咨询'
        }
    },
    {
      path: "/home/newinfo/:id",
      name: "NewsInfo",
      component: NewsInfo,
        meta: {
          title: '新闻详情'
        }
    },
    {
      path: "/home/PhotoList",
      name: "PhotoList",
      component: PhotoList,
        meta: {
          title: '图片分享'
        }
    },
    {
      path: "/home/PhotoInfo/:id",
      name: "PhotoInfo",
      component: PhotoInfo,
        meta: {
          title: '图片详情'
        }
    },
    {
      path: "/home/goodslist",
      name: "GoodsList",
      component: GoodsList,
        meta: {
          title: '商品购买'
        }
    },
    {
      path: "/home/goodsinfo/:id",
      name: "GoodsInfo",
      component: GoodsInfo,
        meta: {
          title: '商品详情'
        }
    },
    {
      path: "/home/goodsdesc/:id/:name",
      name: "GoodsDesc",
      component: GoodsDesc,
        meta: {
          title: '商品详情'
        }
    },
    {
      path: "/home/goodscomment/:id",
      name: "GoodsComment",
      component: GoodsComment
    },
    {
      path: "/home/messageBack",
      name: "MessageBack",
      component: MessageBack,
       meta: {
         title: '留言反馈'
       }
    },
     {
       path: "/home/videozq",
       name: "Videozq",
       component: Videozq,
       meta: {
         title: '视频专区'
       }
     },
     {
       path: "/home/contactus",
       name: "ContactUs",
       component: ContactUs,
       meta: {
         title: '联系我们'
       }
     },
  ],
  linkActiveClass: "mui-active"
});
//把路由对象暴露出去
export default router;