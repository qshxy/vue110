//入口文件
import Vue from 'vue';

//按需导入header
/* import {Header,Swipe, SwipeItem,Button,Lazyload } from 'mint-ui'
Vue.component(Header.name,Header);
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);
Vue.use(Lazyload); */
/* 全局导入 */
import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css';
Vue.use(MintUI);
//导入mui组件
import './lib/mui/css/mui.min.css';
import './lib/mui/css/icons-extra.css';
/* 导入vue-preview的css */
import './css/golbal.css'
//导入路由组件
//1.导入vue-router
import VueRouter from 'vue-router';
//2.手动安装VueRouter
Vue.use(VueRouter);
//导入 vue-resource 组件
import VueResource from 'vue-resource';
//在vue中使用 vue-resource 组件
Vue.use(VueResource);
//注册vuex
import Vuex from 'vuex'
Vue.use(Vuex)
//在网站加载的时候，首先加载main.js,所以最开始car从本地存储取，
var car = JSON.parse(localStorage.getItem('car') || '[]')
//新建一个vuex的store存储
var store = new Vuex.Store({
    state: { //this.$store.state.***
        car: car,//将购物车中的数据用一个数组存起来，在car数组中存放一些商品的对象，
        //咱们可以暂时将商品的对象设计成这样
        //{id：商品的id，count：商品的购买数量，price：商品的加个，selected：false}
        cou:12
    },
    mutations: { //this.$store.commit("方法名",按需传入的唯一参数值)
        addToCar(state, goodscar) {
            // 点击加入购物车，把商品信息，保存到 store 中的 car 上
            // 分析：
            // 1. 如果购物车中，之前就已经有这个对应的商品了，那么，只需要更新数量
            // 2. 如果没有，则直接把 商品数据，push 到 car 中即可

            // 假设 在购物车中，没有找到对应的商品
            var flag = false

            state.car.some(item => {
                if (item.id == goodscar.id) {
                    item.count += parseInt(goodscar.count)
                    flag = true
                    return true
                }
            })

            // 如果最终，循环完毕，得到的 flag 还是 false，则把商品数据直接 push 到 购物车中
            if (!flag) {
                state.car.push(goodscar)
            }
            //将car存储到本地存储
            localStorage.setItem('car',JSON.stringify(state.car))
        },
        updateCount(state,goodsinfo){
            /* 修改购物车中的数据 */
            state.car.some(item => {
                if(item.id==goodsinfo.id){
                    item.count = parseInt(goodsinfo.count);
                    return true;
                }
            });
            //当修改完商品的数量，把最新的数据保存到本地存储中
            localStorage.setItem('car',JSON.stringify(state.car))
        },
        removeCar(state,id){
            //删除数据ing
            //根据id从store中删除对应的商品
           state.car.some((item,i)=>{
               if(item.id==id){
                   state.car.splice(1,i);
                   return true
               }
           })
             //当修改完商品的数量，把最新的数据保存到本地存储中
             localStorage.setItem('car',JSON.stringify(state.car))
        },
        updatedSelected(state,obj){
            state.car.some(item=>{
                if(item.id==obj.id){
                    item.selected=obj.selected;
                    return true
                }
            })
            //当修改完商品的状态，把最新的数据保存到本地存储中
            localStorage.setItem('car', JSON.stringify(state.car))
        }
    },
    getters: { //this.$store.getters.***
            getAllCount(state){
                //定义中间变量存储所有的cout总和，默认为0
                var c=0;
                //循环car数组中的每一项，获取每一项中的count
                state.car.forEach(item=>{
                    c+=item.count
                })
                return c;
            },
            getgoodsAllcount(state){
                var o={};
                state.car.forEach(item=>{
                     //用对象的键值来对应到相应count
                     o[item.id] = item.count;
                })
                return o;//返回带有id和count的对象
            },
            getgoodsSelected(state){
                var o={}//根据id存储switch的状态
                state.car.forEach(item=>{
                    o[item.id] = item.selected;
                })
                return o;
            },
            //数据的改变在getters中写方法
            getgoodsAmount(state){//根据选出的商品计算件数和总价
            //首先要建一个个存放件数和总价的个对象，方便判断取值
             var o={
                 count:0,//件数
                 amount:0//总价格
             }
             //循环每一项来进行判断
             state.car.forEach(item=>{
                 if(item.selected){
                     o.count+=item.count;
                     o.amount+=item.count*item.price;
                 }
                
             })
                return o

            }
    }
})
//设置全局请求路径
Vue.http.options.root = 'http://vue.studyit.io';
//定义全局表单提交格式
Vue.http.options.emulateJSON = true;
//导入格式化时间插件
import moment from 'moment';
// 定义全局时间过滤器
Vue.filter('dataFormat', function (dataStr, paTime = 'YYYY-MM-DD') {
    return moment(dataStr).format(paTime);
})
//导入图片缩略图插件
import VuePreview from 'vue-preview'
// defalut install
Vue.use(VuePreview)
//vue 设置网页title的问题
import VueWechatTitle from 'vue-wechat-title';
Vue.use(VueWechatTitle)
//导入自定义路由模块
import router from './router.js';

/* 导入app组件 */
import app from './App.vue';
var vm = new Vue({
    el: '#app',
    render: c => c(app), //将app组件通过render函数渲染到页面
    router, //挂载路由
    store //挂载数据存储库

})